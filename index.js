/*  
 *          ****************************
 *          * Curle starting rewrite:  *
 *          * discord.io to discord.js *
 *          ****************************
 *          * Current date is 19/10/18 *
 *          ****************************
 *  
 * 
 *          Notes: This file is mostly proprietary communication.
 *                 No modification required here.
 * 
 * 
 */

/**
 * If 'debug', then some additional debugging output will be logged.
 * Otherwise, nothing.
 */
process.env.NODE_ENV = 'debug';
package = require('./package.json');

console.log('Starting ' + package.name + ' v' + package.version);

// Configure logging for context 'main'
logger = require('./logger').get('main');

// Construct Discord client
logger.info('Initializing Discord client.');
discord_client = require('./discord_client');
logger.info('Discord Prepared.');

// Construct IRC client
logger.info('Initializing IRC client.');
irc_client = require('./irc_client');
logger.info('IRC Ready.');

const Community = require('./Community');
// Construct a new community
logger.info('Initializing Community.');
community = new Community();
logger.info('Community Initialized.');

logger.info('Adding Participants to Community.');
community.add(discord_client);
community.add(irc_client);
logger.info('Community Ready.');

// More clients and communities may be constructed as shown here
