// Configure logging for context 'irc_client'
var logger = require('./logger').get('irc_client');

logger.info('Preparing IRC');
const Participant = require('./Participant');
const UMessage = require('./UMessage');
var irc = require('irc');
var auth = require('./auth');
logger.info('IRC Prepared');

// TODO: move this into a JSON file, loop through it, and create as many server connections as we specify
// TODO: open an issue about this and the similar comment in discord_client.js
var server = 'irc.garfielf.net';
var nick = "";
if(auth.sub === "true") {
	nick = "subroson";
} else if (auth.sub === "false") {
	nick = "superroson";
} ;

//var nick = 'subroson';
var options = {};

const Client = new irc.Client(server, nick, options);

logger.info(Client.displayName);


logger.info('Creating new IRC client');
/**
 * Instantiate {@link Participant} with a new IRC client and a method that takes a {@link UMessage} and sends it to the right channel(s)
 */
irc_client = new Participant('irc_client', 'irc', Client, function(message) {
	// pretty rudimentary check to make sure we don't send the message right back where it came from
	// NOTE: this assumes that the client can only be in one channel with this name. if this is the case
	if(message.source != this.id) {
		// send it wooo
		this.participant.say(message.channel.displayName, message.user.displayName + ': ' + message.content);
	}

}, { // options for constructor
	join_callback: function(community) {this.refresh_channels();}, // IRC requires an explicit list of channels to join, so if we get new ones, we'd like to know about it
	refresh_channels: function() {
		this.communities.forEach(community => {
			community.participants.forEach(party => {
				party.channels.forEach(channel => {
					this.participant.join(channel, (nicks) => {this.channels.push(channel);});
				}); // end of channels
			}); // end of participants
		}); // end of communities
	}, // end of refresh_channels

	/**
	 * If another {@link Participant} joins the {@link Community}, this will get called. See reasoning on `join_callback`.
	 */
	reload: function() {this.refresh_channels();}
});
irc_client.participant.logger = logger;
logger.info('IRC Client Initialized');

// I guess there's not much to do here
irc_client.participant.addListener('ready', function (evt) {
	logger.info('IRC Client Ready');
});

irc_client.participant.addListener('error', function (message) {
	logger.error("error: " + message);
});

irc_client.participant.addListener('debug', function(message) {
	logger.debug(message);
});
/**
 * Whenever the IRC client receives a 'message#' (see docs for this client implementation), this method will be called. It does two things:
 *     * Convert the IRC message into a {@link UMessage}
 *     * Pass the UMessage to the {@link Participant}.propagate method
 * TODO: Add links here to documentation on this IRC client implementation. At least remembering the name would be nice. Ah, laziness.
 */
irc_client.participant.addListener('message#', function (nick, to, text, msg) {
	// ignore our own messages
	if(nick == "subroson" || nick == "superroson") return;

	// prep message so we don't have to do stuff the hard way :P
	msg.to = to;
	msg.text = text;

	logger.info("new irc message from " + to + " saying " + msg);

	var message = new UMessage(msg,'irc',this.parent.id);
	this.parent.propagate(message);

});

module.exports = irc_client;
